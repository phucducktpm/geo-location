package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	fmt.Println("geo location")
	mainGeo()
}

type (
	GeoDataRequest struct {
		DateToken string `json:"date_token"`
		Latitude  string `json:"latitude"`
		Longitude string `json:"longitude"`
	}
	GeoDataResponse struct {
		req  GeoDataRequest
		resp DateResponse
	}
	DateResponse struct {
		Month     int    `json:"month"`
		Date      string `json:"date"`
		Year      int    `json:"year"`
		Address   string `json:"address"`
		Day       int    `json:"day"`
		IsWeekend bool   `json:"is_weekend"`
	}
	Output struct {
		Items []struct {
			Title string `json:"title"`
		} `json:"items"`
	}
)

func readcsv(path string) (records [][]string, err error) {
	f, err := os.Open(path)
	if err != nil {
		return records, err
	}

	defer f.Close()
	csvReader := csv.NewReader(f)
	return csvReader.ReadAll()
}

func getDate(inTime string) DateResponse {
	var resp DateResponse
	pTime, _ := time.Parse("2006-01-02 15:04:05", inTime)
	resp.Date = pTime.Weekday().String()
	if resp.Date == "Sunday" {
		resp.IsWeekend = true
	}

	year, month, day := pTime.Date()
	resp.Year = year
	resp.Day = day
	resp.Month = int(month)
	return resp
}

func requestHandler(location GeoDataRequest) (address string) {
	url := "https://revgeocode.search.hereapi.com/v1/revgeocode?apiKey=5Ctpl3hfGeYI2dn1Uy8U1t24g9cBN_BmHo_Ngdog9KY&at=" + fmt.Sprint(location.Latitude) + "," + fmt.Sprint(location.Longitude)
	res, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err)
	}
	var data Output
	json.Unmarshal(body, &data)
	for _, add := range data.Items {
		address = add.Title
	}
	return address

}
func parserLocation(records [][]string) (locations []GeoDataRequest) {
	locations = make([]GeoDataRequest, len(records))
	for i, record := range records {
		if len(record) < 3 {
			continue
		}
		locations[i] = GeoDataRequest{
			DateToken: record[0],
			Latitude:  record[1],
			Longitude: record[2],
		}
	}
	return locations
}

func mainGeo() {
	records, err := readcsv("./test.csv")
	if err != nil {
		log.Fatal("error while read file csv", err)
	}

	locations := parserLocation(records)
	respData := make([]GeoDataResponse, len(locations))
	for i, location := range locations {
		address := requestHandler(location)
		respTime := getDate(location.DateToken)
		respTime.Address = address
		respData[i].req = location
		respData[i].resp = respTime
	}
	fmt.Println(respData)
}
